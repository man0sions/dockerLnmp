# docker-lnmp

结构示意图：

![Demo Image](./docker.png)



> php7.1.11 mysql5.6 redis4.0

## 1. 快速使用
1. 本地安装`git`、`docker`和`docker-compose`。
2. `clone`项目：
    ```
    $ git clone git@gitee.com:man0sions/dockerLnmp.git
    ```
3. 启动：
    ```
    $ cd docker-lnmp
    $ docker-compose up
    或者
    $ docker-compose up -d 后台运行
    
    建议在centos 7以上运行，mac无限制
    
    centos 7 以下启动：
    docker-compose -f docker-compose-v1.1.yml up

    ```
4. 在浏览器中访问 `http://localhost/index.php`：










